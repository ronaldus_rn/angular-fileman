import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FileManagerModule} from 'ng6-file-man';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
// import { fas } from '@fortawesome/free-solid-svg-icons';
// import { far } from '@fortawesome/free-regular-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccordionComponent } from './accordion/accordion.component';
import { FileManagerComponent } from './file-manager/file-manager.component';

//
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoMaterialModule} from '../../src/material-module';

//

@NgModule({
  declarations: [
    AppComponent,
    AccordionComponent,
    FileManagerComponent,
    
  ],
  imports: [
    BrowserModule,
    FileManagerModule,
    FontAwesomeModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}

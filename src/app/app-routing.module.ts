import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccordionComponent } from './accordion/accordion.component';
import { FileManagerComponent } from 'ng6-file-man';


const routes: Routes = [
  { path: 'accordion', component: AccordionComponent},
  { path: 'filedude', component: FileManagerComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

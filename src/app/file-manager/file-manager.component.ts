import { Component, OnInit } from '@angular/core';
import { ConfigInterface, TreeModel} from 'ng6-file-man';

@Component({
  selector: 'app-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss']
})
export class FileManagerComponent implements OnInit {

 

  tree: TreeModel;
  appLanguage = 'en';


  constructor()    
  {
    const treeConfig: ConfigInterface = {
    baseURL: 'http://localhost:4200/',
    api: {
      listFile: 'api/proxied-files/filedude/api/list',
      uploadFile: 'api/proxied-files/filedude/api/upload',
      downloadFile: 'api/proxied-files/filedude/api/download',
      deleteFile: 'api/proxied-files/filedude/api/remove',
      createFolder: 'api/proxied-files/filedude/api/directory',
      renameFile: 'api/proxied-files/filedude/api/rename',
      searchFiles: 'api/proxied-files/filedude/api/search'
    },
    options: {
      allowFolderDownload: false,
      showFilesInsideTree: false
    }
  };

  this.tree = new TreeModel(treeConfig);
}

itemClicked(event: any) {
  console.log(event);
}

  ngOnInit() {
  }

}

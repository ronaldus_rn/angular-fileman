import { Component } from '@angular/core';
import {ConfigInterface, TreeModel} from 'ng6-file-man';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'fileMan';
  constructor() { }

}
